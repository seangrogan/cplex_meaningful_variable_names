# README #

This is the sample code for meaningful variable names in CPLEX.  If you build a model in CPLEX using C++, variable names will often have difficult to follow variable names.  This sample code should assist another user in setting up meaningful variable names in CPLEX.  

The model has a number of binary decisions that are made over a span of a number of years.  

### What is this repository for? ###

This is simply a sample code 

### Who do I talk to? ###

* Sean Grogan
* sean.grogan@gmail.com