//*************************************************************************************************
//********** Beginning of the sample code using C++ ***********************************************
//********** This is the sample code for meaningful variable names in CPLEX ***********************
//*************************************************************************************************

std::cout << "\tCreating Variables\n";

// Making a number of binary decison variables for the number of elements in Model
// Each decsion variable in the model has a separate decision that cen be made over 
// the next NumYears periods.  I.e. we have a matrix of binary decision variables 
// in the 2-D array binary[i][j]  with a size Model.size() by NumYears

IloArray<IloBoolVarArray> binary(env, Model.size());
for( IloInt i = 0; i < Model.size(); i++ )
    binary[i] = IloBoolVarArray( env, NumYears );
    
// The following code allows for meaningful variable names in the exported *.LP 
// file in from the CPLEX enviroment

char *buffer= new char [200];
for(int i = 0; i < Model.size(); i++){
    for(int j = 0; j < NumYears; j++){
        sprintf (buffer,"MeaningfulName{%d,%d}",i,j);
		
        // This is the line that sets the binary variable name.  
        binary[i][j].setName(buffer);
    }
}

// Now when you export the *.LP model to file, you'll see the binay variables as 
// MeanigfulName{5,3} for the fifth decision in the third year

// Good luck and happy optimizing!